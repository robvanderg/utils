#include "evalb.ih"

Evalb::Evalb(istream *gold, istream *parser, bool verbose)
//:
{
    readParameterFile("evalb/sample.prm");

    d_verbose = verbose;
    TOTAL_bn1 = TOTAL_bn2 = TOTAL_match = 0;
    TOTAL_sent = TOTAL_error_sent = TOTAL_skip_sent = TOTAL_comp_sent = 0;
    TOTAL_word = TOTAL_correct_tag = 0;
    TOTAL_crossing = 0;
    TOTAL_no_crossing = TOTAL_2L_crossing = 0;

    TOT40_bn1 = TOT40_bn2 = TOT40_match = 0;
    TOT40_sent = TOT40_error_sent = TOT40_skip_sent = TOT40_comp_sent = 0;
    TOT40_word = TOT40_correct_tag = 0;
    TOT40_crossing = 0;
    TOT40_no_crossing = TOT40_2L_crossing = 0;

    char buff[5000];
    char buff1[5000];

    // Read data
    if (d_verbose)
        printHead();
    for(;gold->getline(buff, 5000) && parser->getline(buff1, 5000); ++Line)
    {
        init();

        r_wn1 = readLine(buff, terminal1, quotterm1, &wn1, bracket1, &bn1);
        readLine(buff1, terminal2, quotterm2, &wn2, bracket2, &bn2);

        calcResult(buff, buff1);
    }
    if (d_verbose)
        printTotal();
    else
    { 
        double r = TOTAL_bn1>0 ? 100.0*TOTAL_match/TOTAL_bn1 : 0.0;
        double p = TOTAL_bn2>0 ? 100.0*TOTAL_match/TOTAL_bn2 : 0.0;
        double f = 2*p*r/(p+r);
        printf("Bracketing FMeasure       = %6.2f\n",f);
    }
}

