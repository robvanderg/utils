#include "evalb.ih"

void Evalb::massageData()
{
    int i, j;

    /* for GOLD */
    /*----------*/
    for(i=0;i<bn1;i++){

        bracket1[i].result = 0;

        /* Zero element */
        if(bracket1[i].start == bracket1[i].end){
            bracket1[i].result = 5;
            continue;
        }

        /* Modify label */
        modifyLabel(bracket1[i].label);

        /* Delete label */
        for(j=0;j<Delete_label_n;j++){
            if(labelComp(bracket1[i].label,Delete_label[j])==1){
                bracket1[i].result = 5;
            }
        }
    }
    /* for TEST */
    /*----------*/
    for(i=0;i<bn2;i++){

        bracket2[i].result = 0;

        /* Zero element */
        if(bracket2[i].start == bracket2[i].end){
            bracket2[i].result = 5;
            continue;
        }

        /* Modify label */
        modifyLabel(bracket2[i].label);

        /* Delete label */
        for(j=0;j<Delete_label_n;j++){
            if(labelComp(bracket2[i].label,Delete_label[j])==1){
                bracket2[i].result = 5;
            }
        }
    }
    /* count up real number of brackets (exclude deleted ones) */
    /*---------------------------------------------------------*/
    r_bn1 = r_bn2 = 0;

    for(i=0;i<bn1;i++){
        if(bracket1[i].result != 5){
            r_bn1++;
        }
    }

    for(i=0;i<bn2;i++){
        if(bracket2[i].result != 5){
            r_bn2++;
        }
    }

}
