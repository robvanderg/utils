#ifndef INCLUDED_EVALB_
#define INCLUDED_EVALB_
#include <fstream>

#define MAX_SENT_LEN           5000
#define MAX_WORD_IN_SENT        200
#define MAX_BRACKET_IN_SENT     200
#define MAX_WORD_LEN            100
#define MAX_LABEL_LEN            30
#define MAX_QUOTE_TERM           20

#define MAX_DELETE_LABEL        100
#define MAX_EQ_LABEL            100
#define MAX_EQ_WORD             100

#define MAX_LINE_LEN            500

#define DEFAULT_MAX_ERROR        10
#define DEFAULT_CUT_LEN          40


typedef struct ss_terminal {
    char word[MAX_WORD_LEN];
    char label[MAX_LABEL_LEN];
    int  result;                /* 0:unmatch, 1:match, 9:undef */
} s_terminal;

typedef struct ss_term_ind {
        s_terminal term;
        int index;
   int bracket;
   int endslen;
   int ends[MAX_BRACKET_IN_SENT];
} s_term_ind;

typedef struct ss_bracket {
    int start;
    int end;
    unsigned int buf_start;
    unsigned int buf_end;
    char label[MAX_LABEL_LEN];
    int  result;                 /* 0: unmatch, 1:match, 5:delete 9:undef */
} s_bracket;


typedef struct ss_equiv {
    char *s1;
    char *s2;
} s_equiv;


class Evalb
{
    bool d_verbose;
    /****************************/
    /* global variables         */
    /*   gold-data: suffix = 1  */
    /*   test-data: suffix = 2  */
    /****************************/
    
    /*---------------*/
    /* Sentence data */
    /*---------------*/
    int wn1, wn2;                              /* number of words in sentence  */
    int r_wn1;                                 /* number of words in sentence  */
                                               /* which only ignores labels in */
                                               /* DELETE_LABEL_FOR_LENGTH      */
    
    s_terminal terminal1[MAX_WORD_IN_SENT];    /* terminal information */
    s_terminal terminal2[MAX_WORD_IN_SENT];
    
    s_term_ind quotterm1[MAX_QUOTE_TERM];      /* special terminals ("'","POS") */
    s_term_ind quotterm2[MAX_QUOTE_TERM];
    
    int bn1, bn2;                              /* number of brackets */
    
    int r_bn1, r_bn2;                          /* number of brackets */
                                               /* after deletion */
    
    s_bracket bracket1[MAX_BRACKET_IN_SENT];   /* bracket information */
    s_bracket bracket2[MAX_BRACKET_IN_SENT];
    
    /*------------*/
    /* Total data */
    /*------------*/
    int TOTAL_bn1, TOTAL_bn2, TOTAL_match;     /* total number of brackets */
    int TOTAL_sent;                            /* No. of sentence */
    int TOTAL_error_sent;                      /* No. of error sentence */
    int TOTAL_skip_sent;                       /* No. of skip sentence */
    int TOTAL_comp_sent;                       /* No. of complete match sent */
    int TOTAL_word;                            /* total number of word */
    int TOTAL_crossing;                        /* total crossing */
    int TOTAL_no_crossing;                     /* no crossing sentence */
    int TOTAL_2L_crossing;                     /* 2 or less crossing sentence */
    int TOTAL_correct_tag;                     /* total correct tagging */
    
    int TOT_cut_len = DEFAULT_CUT_LEN;         /* Cut-off length in statistics */
    
                                     /* data for sentences with len <= CUT_LEN */
                                     /* Historically it was 40.                */
    int TOT40_bn1, TOT40_bn2, TOT40_match;     /* total number of brackets */
    int TOT40_sent;                            /* No. of sentence */
    int TOT40_error_sent;                      /* No. of error sentence */
    int TOT40_skip_sent;                       /* No. of skip sentence */
    int TOT40_comp_sent;                       /* No. of complete match sent */
    int TOT40_word;                            /* total number of word */
    int TOT40_crossing;                        /* total crossing */
    int TOT40_no_crossing;                     /* no crossing sentence */
    int TOT40_2L_crossing;                     /* 2 or less crossing sentence */
    int TOT40_correct_tag;                     /* total correct tagging */
    
    /*------------*/
    /* miscallous */
    /*------------*/
    int Line;                                  /* line number */
    int Error_count = 0;                       /* Error count */
    int Status;                                /* Result status for each sent */
                                               /*    0: OK, 1: skip, 2: error */
    
    /*-------------------*/
    /* stack manuplation */
    /*-------------------*/
    int stack_top;
    int stack[MAX_BRACKET_IN_SENT];
    
    /************************************************************/
    /* User parameters which can be specified in parameter file */
    /************************************************************/
    
    /*------------------------------------------*/
    /* Debug mode                               */
    /*   print out data for individual sentence */
    /*------------------------------------------*/
    int DEBUG=0;
    
    /*------------------------------------------*/
    /* MAX error                                */
    /*    Number of error to stop the process.  */
    /*    This is useful if there could be      */
    /*    tokanization error.                   */
    /*    The process will stop when this number*/
    /*    of errors are accumulated.            */
    /*------------------------------------------*/
    int Max_error = DEFAULT_MAX_ERROR;
    
    /*------------------------------------------*/
    /* Cut-off length for statistics            */
    /*    int TOT_cut_len = DEFAULT_CUT_LEN;    */
    /*    (Defined above)                       */
    /*------------------------------------------*/
    
    
    /*------------------------------------------*/
    /* unlabeled or labeled bracketing          */
    /*    0: unlabeled bracketing               */
    /*    1: labeled bracketing                 */
    /*------------------------------------------*/
    int F_label    = 1;
    
    /*------------------------------------------*/
    /* Delete labels                            */
    /*    list of labels to be ignored.         */
    /*    If it is a pre-terminal label, delete */
    /*    the word along with the brackets.     */
    /*    If it is a non-terminal label, just   */
    /*    delete the brackets (don't delete     */
    /*    childrens).                           */
    /*------------------------------------------*/
    char *Delete_label[MAX_DELETE_LABEL];
    int Delete_label_n = 0;
    
    /*------------------------------------------*/
    /* Delete labels for length calculation     */
    /*    list of labels to be ignored for      */
    /*    length calculation purpose            */
    /*------------------------------------------*/
    char *Delete_label_for_length[MAX_DELETE_LABEL];
    int Delete_label_for_length_n = 0;
    
    /*------------------------------------------*/
    /* Labels to be considered for misquote     */
    /*    (could be possesive or quote)         */
    /*------------------------------------------*/
    char *Quote_term[MAX_QUOTE_TERM];
    int Quote_term_n = 0;
    
    /*------------------------------------------*/
    /* Equivalent labels, words                 */
    /*     the pairs are considered equivalent  */
    /*     This is non-directional.             */
    /*------------------------------------------*/
    s_equiv EQ_label[MAX_EQ_LABEL];
    int EQ_label_n = 0;
    
    s_equiv EQ_word[MAX_EQ_WORD];
    int EQ_word_n = 0;

    public:
        Evalb(std::istream *gold, std::istream *parser, bool verbose);

        int readLine(char *buff, s_terminal terminal[], s_term_ind quotterm[], int *wn, s_bracket bracket[], int *bn);
        int isTerminator(char c);
        int isDeletelabelForLength(char *s);
        int isDeletelabel(char *s);
        int isQuoteTerm(char *s, char *w);
        void error(char const *s);
        void pushB(int item);
        int popB();
        int stackEmpty();
        void calcResult(char *buf1, char *bug);
    
        void individualResult(int wn1, int bn1, int bn2, int match, int crossing, int correct_tag);
        void printTotal();
        void fixQuote();
        void reinsertTerm(s_term_ind *quot, s_terminal terminal[], s_bracket bracket[], int *wn);
        void massageData();
        void modifyLabel(char *label);
        int labelComp(char *s1, char *s2);
        void init();
        void readParameterFile(char const *filename);
        void setParam(char *param, char *value);
        int narg(char *s);

        void printHead();

    private:
};

#endif
