#include "evalb.ih"

void Evalb::individualResult(int wn1, int bn1, int bn2, int match, int crossing, int correct_tag)
{
    TOTAL_sent++;
    if(Status==1){
        TOTAL_error_sent++;
    }else if(Status==2){
        TOTAL_skip_sent++;
    }else{
        TOTAL_bn1 += bn1;
        TOTAL_bn2 += bn2;
        TOTAL_match += match;
        if(bn1==bn2 && bn2==match){
            TOTAL_comp_sent++;
        }
        TOTAL_word += wn1;
        TOTAL_crossing += crossing;
        if(crossing==0){
            TOTAL_no_crossing++;
        }
        if(crossing <= 2){
            TOTAL_2L_crossing++;
        }
        TOTAL_correct_tag += correct_tag;
    }


    /* Statistics for sent length <= TOT_cut_len */
    /*-------------------------------------------*/
    if(r_wn1<=TOT_cut_len){
        TOT40_sent++;
        if(Status==1){
            TOT40_error_sent++;
        }else if(Status==2){
            TOT40_skip_sent++;
        }else{
            TOT40_bn1 += bn1;
            TOT40_bn2 += bn2;
            TOT40_match += match;
            if(bn1==bn2 && bn2==match){
                TOT40_comp_sent++;
            }
            TOT40_word += wn1;
            TOT40_crossing += crossing;
            if(crossing==0){
                TOT40_no_crossing++;
            }
            if(crossing <= 2){
                TOT40_2L_crossing++;
            }
            TOT40_correct_tag += correct_tag;
        }
    }
    /* Print individual result */
    /*-------------------------*/
    if (d_verbose)
    {
        printf("%4d  %3d    %d  ",Line,r_wn1,Status);
        printf("%6.2f %6.2f   %3d    %3d  %3d    %3d",
               (r_bn1==0?0.0:100.0*match/r_bn1),
               (r_bn2==0?0.0:100.0*match/r_bn2),
               match, r_bn1, r_bn2, crossing);

        printf("   %4d  %4d   %6.2f\n",wn1,correct_tag,
               (wn1==0?0.0:100.0*correct_tag/wn1));
    }
}
