#include "evalb.ih"

void Evalb::readParameterFile(char const *filename)
{
    char buff[MAX_LINE_LEN];
    FILE *fd;
    int line;
    int i;

    if((fd=fopen(filename,"r"))==NULL){
        error("Can't open parameter file (%s)\n");//,filename); //TODO
    }

    for(line=1;fgets(buff,MAX_LINE_LEN,fd)!=NULL;line++){

      /* clean up the tail and find unvalid line */
      /*-----------------------------------------*/
        for(i=strlen(buff)-1;i>0 && (isspace(buff[i]) || buff[i]=='\n');i--){
            buff[i]='\0';
        }
        if(buff[0]=='#' ||      /* comment-line */
           strlen(buff)<3){     /* too short, just ignore */
            continue;
        }

      /* place the parameter and value */
      /*-------------------------------*/
        for(i=0;!isspace(buff[i]);i++);
        for(;isspace(buff[i]) && buff[i]!='\0';i++);
        if(buff[i]=='\0'){
            fprintf(stderr,"Empty value in parameter file (%d)\n",line);
        }

      /* set parameter and value */
      /*-------------------------*/
        setParam(buff,buff+i);
    }

    fclose(fd);

}
