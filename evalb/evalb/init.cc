#include "evalb.ih"

void Evalb::init()
{
      int i;

  wn1 = 0;
  wn2 = 0;
  bn1 = 0;
  bn2 = 0;
  r_bn1 = 0;
  r_bn2 = 0;

  for(i=0;i<MAX_WORD_IN_SENT;i++){
      terminal1[i].word[0]  = '\0';
      terminal1[i].label[0] = '\0';
      terminal1[i].result   = 9;
      terminal2[i].word[0]  = '\0';
      terminal2[i].label[0] = '\0';
      terminal2[i].result   = 9;
  }

  for(i=0;i<MAX_QUOTE_TERM;i++){
      quotterm1[i].term.word[0]  = '\0';
      quotterm1[i].term.label[0] = '\0';
      quotterm1[i].term.result   = 9;
      quotterm1[i].index         = -1;
      quotterm1[i].bracket       = -1;
      quotterm2[i].term.word[0]  = '\0';
      quotterm2[i].term.label[0] = '\0';
      quotterm2[i].term.result   = 9;
      quotterm2[i].index         = -1;
      quotterm2[i].bracket       = -1;
  }

  for(i=0;i<MAX_BRACKET_IN_SENT;i++){
      bracket1[i].start    = -1;
      bracket1[i].end      = -1;
      bracket1[i].label[0] = '\0';
      bracket1[i].result   = 9;
      bracket2[i].start    = -1;
      bracket2[i].end      = -1;
      bracket2[i].label[0] = '\0';
      bracket2[i].result   = 9;
  }

  Status = 0;
}

