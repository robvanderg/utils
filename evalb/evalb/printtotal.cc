#include "evalb.ih"

void Evalb::printTotal()
{
        int sentn;
    double r,p,f;

    printf("============================================================================\n");

    if(TOTAL_bn1>0 && TOTAL_bn2>0){
        printf("                %6.2f %6.2f %6d %5d %5d  %5d",
               (TOTAL_bn1>0?100.0*TOTAL_match/TOTAL_bn1:0.0),
               (TOTAL_bn2>0?100.0*TOTAL_match/TOTAL_bn2:0.0),
               TOTAL_match,
               TOTAL_bn1,
               TOTAL_bn2,
               TOTAL_crossing);
    }

    printf("  %5d %5d   %6.2f",
           TOTAL_word,
           TOTAL_correct_tag,
           (TOTAL_word>0?100.0*TOTAL_correct_tag/TOTAL_word:0.0));

    printf("\n");
    printf("=== Summary ===\n");

    sentn = TOTAL_sent - TOTAL_error_sent - TOTAL_skip_sent;

    printf("\n-- All --\n");
    printf("Number of sentence        = %6d\n",TOTAL_sent);
    printf("Number of Error sentence  = %6d\n",TOTAL_error_sent);
    printf("Number of Skip  sentence  = %6d\n",TOTAL_skip_sent);
    printf("Number of Valid sentence  = %6d\n",sentn);

    r = TOTAL_bn1>0 ? 100.0*TOTAL_match/TOTAL_bn1 : 0.0;
    printf("Bracketing Recall         = %6.2f\n",r);

    p = TOTAL_bn2>0 ? 100.0*TOTAL_match/TOTAL_bn2 : 0.0;
    printf("Bracketing Precision      = %6.2f\n",p);

    f = 2*p*r/(p+r);
    printf("Bracketing FMeasure       = %6.2f\n",f);

    printf("Complete match            = %6.2f\n",
           (sentn>0?100.0*TOTAL_comp_sent/sentn:0.0));
    printf("Average crossing          = %6.2f\n",
           (sentn>0?1.0*TOTAL_crossing/sentn:0.0));
    printf("No crossing               = %6.2f\n",
           (sentn>0?100.0*TOTAL_no_crossing/sentn:0.0));
    printf("2 or less crossing        = %6.2f\n",
           (sentn>0?100.0*TOTAL_2L_crossing/sentn:0.0));
    printf("Tagging accuracy          = %6.2f\n",
           (TOTAL_word>0?100.0*TOTAL_correct_tag/TOTAL_word:0.0));

    sentn = TOT40_sent - TOT40_error_sent - TOT40_skip_sent;

    printf("\n-- len<=%d --\n",TOT_cut_len);
    printf("Number of sentence        = %6d\n",TOT40_sent);
    printf("Number of Error sentence  = %6d\n",TOT40_error_sent);
    printf("Number of Skip  sentence  = %6d\n",TOT40_skip_sent);
    printf("Number of Valid sentence  = %6d\n",sentn);


    r = TOT40_bn1>0 ? 100.0*TOT40_match/TOT40_bn1 : 0.0;
    printf("Bracketing Recall         = %6.2f\n",r);

    p = TOT40_bn2>0 ? 100.0*TOT40_match/TOT40_bn2 : 0.0;
    printf("Bracketing Precision      = %6.2f\n",p);

    f = 2*p*r/(p+r);
    printf("Bracketing FMeasure       = %6.2f\n",f);

    printf("Complete match            = %6.2f\n",
           (sentn>0?100.0*TOT40_comp_sent/sentn:0.0));
    printf("Average crossing          = %6.2f\n",
           (sentn>0?1.0*TOT40_crossing/sentn:0.0));
    printf("No crossing               = %6.2f\n",
           (sentn>0?100.0*TOT40_no_crossing/sentn:0.0));
    printf("2 or less crossing        = %6.2f\n",
           (sentn>0?100.0*TOT40_2L_crossing/sentn:0.0));
    printf("Tagging accuracy          = %6.2f\n",
           (TOT40_word>0?100.0*TOT40_correct_tag/TOT40_word:0.0));
}
