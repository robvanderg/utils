#include "evalb.ih"

int Evalb::readLine(char *buff, s_terminal terminal[], s_term_ind quotterm[], int *wn, ss_bracket bracket[], int *bn)
{
    char *p, *q, label[MAX_LABEL_LEN], word[MAX_WORD_LEN];
    int   qt;             /* quote term counter */
    int   wid, bid;       /* word ID, bracket ID */
    int   n;              /* temporary remembering the position */
    int   b;              /* temporary remembering bid */
    int   i;
    int   len;            /* length of the sentence */

    len = 0;
    stack_top=0;

    for(p=buff,qt=0,wid=0,bid=0;*p!='\0';){

        if(isspace(*p)){
            p++;
            continue;

        /* open bracket */
        /*--------------*/
        }else if(*p=='('){

            n=wid;
            for(p++,i=0;!isTerminator(*p);p++,i++){
                label[i]=*p;
            }
            label[i]='\0';

            /* Find terminals */
            q = p;
            if(isspace(*q)){
                for(q++;isspace(*q);q++);
                for(i=0;!isTerminator(*q);q++,i++){
                    word[i]=*q;
                }
                word[i]='\0';

                /* compute length */
                if(*q==')' && !(isDeletelabelForLength(label)==1)){ //TODO
                    len++;
                }
      if (DEBUG>1)
         printf("label=%s, word=%s, wid=%d\n",label,word,wid);
                /* quote terminal */
                if(*q==')' && isQuoteTerm(label,word)==1){
                        strcpy(quotterm[qt].term.word,word);
                        strcpy(quotterm[qt].term.label,label);
                        quotterm[qt].index = wid;
         quotterm[qt].bracket = bid;
         quotterm[qt].endslen = stack_top;
         //quotterm[qt].ends = (int*)malloc(stack_top*sizeof(int));
         memcpy(quotterm[qt].ends,stack,stack_top*sizeof(int));
                        qt++;
                }

                /* delete terminal */
                if(*q==')' && isDeletelabel(label)==1){
                    p = q+1;
                    continue;

                /* valid terminal */
                }else if(*q==')'){
                    strcpy(terminal[wid].word,word);
                    strcpy(terminal[wid].label,label);
                    wid++;
                    p = q+1;
                    continue;

                /* error */
                }else if(*q!='('){
                    error("More than two elements in a bracket\n");
                }
            }

            /* otherwise non-terminal label */
            bracket[bid].start = wid;
            bracket[bid].buf_start = p-buff;
            strcpy(bracket[bid].label,label);
            pushB(bid);
            bid++;

        /* close bracket */
        /*---------------*/
        }else if(*p==')'){

            b = popB();
            bracket[b].end = wid;
            bracket[b].buf_end = p-buff;
            p++;

        /* error */
        /*-------*/
        }else{

            error("Reading sentence\n");
        }
    }

    if(!stackEmpty()){
        error("Bracketing is unbalanced (too many open bracket)\n");
    }
    *wn = wid;
    *bn = bid;

    return(len);
}


