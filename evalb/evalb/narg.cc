#include "evalb.ih"

int Evalb::narg(char *s)
{
    int n;

    for(n=0;*s!='\0';){
        for(;isspace(*s);s++);
        if(*s=='\0'){
            break;
        }
        n++;
        for(;!isspace(*s);s++){
            if(*s=='\0'){
                break;
            }
        }
    }

    return(n);

}
