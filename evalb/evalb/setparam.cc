#include "evalb.ih"


#define STRNCMP(s) (strncmp(param,s,strlen(s))==0 &&  \
                    (param[strlen(s)]=='\0' || isspace(param[strlen(s)])))

void Evalb::setParam(char *param, char *value)
{
        char l1[MAX_LABEL_LEN], l2[MAX_LABEL_LEN];

    if(STRNCMP("DEBUG")){

        DEBUG = atoi(value);

    }else if(STRNCMP("MAX_ERROR")){

        Max_error = atoi(value);

    }else if(STRNCMP("CUTOFF_LEN")){

        TOT_cut_len = atoi(value);

    }else if(STRNCMP("LABELED")){

        F_label = atoi(value);

    }else if(STRNCMP("DELETE_LABEL")){

        Delete_label[Delete_label_n] = (char *)malloc(strlen(value)+1);
        strcpy(Delete_label[Delete_label_n],value);
        Delete_label_n++;

    }else if(STRNCMP("DELETE_LABEL_FOR_LENGTH")){

        Delete_label_for_length[Delete_label_for_length_n] = (char *)malloc(strlen(value)+1);
        strcpy(Delete_label_for_length[Delete_label_for_length_n],value);
        Delete_label_for_length_n++;

    }else if(STRNCMP("QUOTE_LABEL")){

        Quote_term[Quote_term_n] = (char *)malloc(strlen(value)+1);
        strcpy(Quote_term[Quote_term_n],value);
        Quote_term_n++;

    }else if(STRNCMP("EQ_LABEL")){

        if(narg(value)!=2){
            fprintf(stderr,"EQ_LABEL requires two values\n");
            return;
        }
        sscanf(value,"%s %s",l1,l2);
        EQ_label[EQ_label_n].s1 = (char *)malloc(strlen(l1)+1);
        strcpy(EQ_label[EQ_label_n].s1,l1);
        EQ_label[EQ_label_n].s2 = (char *)malloc(strlen(l2)+1);
        strcpy(EQ_label[EQ_label_n].s2,l2);
        EQ_label_n++;
    }else if(STRNCMP("EQ_WORD")){

        if(narg(value)!=2){
            fprintf(stderr,"EQ_WORD requires two values\n");
            return;
        }
        sscanf(value,"%s %s",l1,l2);
        EQ_word[EQ_word_n].s1 = (char *)malloc(strlen(l1)+1);
        strcpy(EQ_word[EQ_word_n].s1,l1);
        EQ_word[EQ_word_n].s2 = (char *)malloc(strlen(l2)+1);
        strcpy(EQ_word[EQ_word_n].s2,l2);
        EQ_word_n++;

    }else{

        fprintf(stderr,"Unknown keyword (%s) in parameter file\n",param);

    }

}
