#include "evalb.ih"

int Evalb::popB()
{
    int item;

    item = stack[stack_top-1];

    if(stack_top-- < 0){
        error("Bracketing unbalance (too many close bracket)\n");
    }
    return(item);
}
