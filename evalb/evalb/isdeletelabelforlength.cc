#include "evalb.ih"

int Evalb::isDeletelabelForLength(char *s)
{
    int i;

    for(i=0;i<Delete_label_for_length_n;i++){
        if(strcmp(s,Delete_label_for_length[i])==0){
            return(1);
        }
    }

    return(0);
}
