#include "evalb.ih"

void Evalb::reinsertTerm(s_term_ind* quot, s_terminal terminal[], s_bracket bracket[], int *wn)
{
       int ind = quot->index;
   int bra = quot->bracket;
   s_terminal* term = &quot->term;
   int k;
   memmove(&terminal[ind+1],
         &terminal[ind],
         sizeof(s_terminal)*(MAX_WORD_IN_SENT-ind-1));
   strcpy(terminal[ind].label,term->label);
   strcpy(terminal[ind].word,term->word);
   (*wn)++;
   if (DEBUG>1)
      printf("bra=%d, ind=%d\n",bra,ind);
   for(k=0;k<MAX_BRACKET_IN_SENT;k++) {
      if (bracket[k].start==-1)
         break;
      if (DEBUG>1)
         printf("bracket[%d]={%d,%d}\n",k,bracket[k].start,bracket[k].end);
      if (k>=bra) {
         bracket[k].start++;
         bracket[k].end++;
      }
      //if (bracket[k].start<=ind && bracket[k].end>=ind)
         //bracket[k].end++;
   }
   if (DEBUG>1)
      printf("endslen=%d\n",quot->endslen);
   for(k=0;k<quot->endslen;k++) {
      //printf("ends[%d]=%d",k,quot->ends[k]);
      bracket[quot->ends[k]].end++;
   }
   //free(quot->ends);
}

