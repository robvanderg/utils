#include "evalb.ih"

void Evalb::fixQuote()
{
       int i,j,k;
   if (DEBUG>1) {
      for(i=0;i<MAX_QUOTE_TERM;i++){
         if (quotterm1[i].index!=-1)
            printf("%d: %s - %s\n",quotterm1[i].index,
                  quotterm1[i].term.label,
                  quotterm1[i].term.word);
         if (quotterm2[i].index!=-1)
            printf("%d: %s - %s\n",quotterm2[i].index,
                  quotterm2[i].term.label,
                  quotterm2[i].term.word);
      }
   }
   for(i=0;i<MAX_QUOTE_TERM;i++) {
      int ind = quotterm2[i].index;
      if (ind!=-1) {
         for(j=0;j<MAX_QUOTE_TERM;j++){
            if (quotterm1[j].index==ind &&
                  strcmp(quotterm1[j].term.label,
                     quotterm2[i].term.label)!=0) {
               if (isDeletelabel(quotterm1[j].term.label) && !isDeletelabel(quotterm2[i].term.label)) {
                  reinsertTerm(&quotterm1[j],terminal1,bracket1,&wn1);
                  for (k=j;k<MAX_QUOTE_TERM;k++)
                     if (quotterm1[k].index!=-1)
                        quotterm1[k].index++;
               } else if (isDeletelabel(quotterm2[i].term.label) && !isDeletelabel(quotterm1[j].term.label)) {
                  reinsertTerm(&quotterm2[i],terminal2,bracket2,&wn2);
                  for (k=i;k<MAX_QUOTE_TERM;k++)
                     if (quotterm2[k].index!=-1)
                        quotterm2[k].index++;
               }
            }
         }
      } else break;
   }

}
