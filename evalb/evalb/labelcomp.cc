#include "evalb.ih"

int Evalb::labelComp(char *s1, char *s2)
{
    int i;

    if(strcmp(s1,s2)==0){
        return(1);
    }

    for(i=0;i<EQ_label_n;i++){
        if((strcmp(s1,EQ_label[i].s1)==0 &&
            strcmp(s2,EQ_label[i].s2)==0) ||
           (strcmp(s1,EQ_label[i].s2)==0 &&
            strcmp(s2,EQ_label[i].s1)==0)){
            return(1);
        }
    }

    return(0);
}
