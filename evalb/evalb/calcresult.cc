#include "evalb.ih"

void Evalb::calcResult(char *buf1, char *buf)
{
    int i, j, l;
    int match, crossing, correct_tag;

    int last_i = -1;

    char my_buf[1000];
    int match_found = 0;

    char match_j[200];
    for (j = 0; j < bn2; ++j) {
      match_j[j] = 0;
    }

    /* ML */
    if (DEBUG>1)
        printf("\n");


    /* Find skip and error */
    /*---------------------*/
    if(wn2==0){
        Status = 2;
        individualResult(0,0,0,0,0,0);
        return;
    }
   if(wn1 != wn2){
      //if (DEBUG>1)
    //Error("Length unmatch (%d|%d)\n",wn1,wn2);
        fixQuote();
        if(wn1 != wn2){
                error("Length unmatch (%d|%d)\n"); //,wn1,wn2); //TODO
                individualResult(0,0,0,0,0,0);
                return;
        }
    }

    for(i=0;i<wn1;i++){
        /*if(word_comp(terminal1[i].word,terminal2[i].word)==0){
            Error("Words unmatch (%s|%s)\n",terminal1[i].word,
                                            terminal2[i].word);
            individual_result(0,0,0,0,0,0);
            return;
        }*/
    }

    /* massage the data */
    /*------------------*/
    massageData();
    /* matching brackets */
    /*-------------------*/
    match = 0;
    for(i=0;i<bn1;i++){
      for(j=0;j<bn2;j++){

         if (DEBUG>1)
   printf("1.res=%d, 2.res=%d, 1.start=%d, 2.start=%d, 1.end=%d, 2.end=%d\n",bracket1[i].result,bracket2[j].result,bracket1[i].start,bracket2[j].start,bracket1[i].end,bracket2[j].end);

        // does bracket match?
        if(bracket1[i].result != 5 &&
           bracket2[j].result == 0 &&
           bracket1[i].start == bracket2[j].start && bracket1[i].end == bracket2[j].end) {

          // (1) do we not care about the label or (2) does the label match?
          if (F_label==0 || labelComp(bracket1[i].label,bracket2[j].label)==1) {
            bracket1[i].result = bracket2[j].result = 1;
            match++;
            match_found = 1;
            break;
          } else {
            if (DEBUG>1) {
              printf("  LABEL[%d-%d]: ",bracket1[i].start,bracket1[i].end-1);
              l = bracket1[i].buf_end-bracket1[i].buf_start;
              strncpy(my_buf,buf1+bracket1[i].buf_start,l);
              my_buf[l] = '\0';
              printf("%s\n",my_buf);
            }
            match_found = 1;
            match_j[j] = 1;
          }
        }
      }

      if (!match_found && bracket1[i].result != 5 && DEBUG>1) {
        /* ### ML 09/28/03: gold bracket with no corresponding test bracket */
        printf("  BRACKET[%d-%d]: ",bracket1[i].start,bracket1[i].end-1);
        l = bracket1[i].buf_end-bracket1[i].buf_start;
        strncpy(my_buf,buf1+bracket1[i].buf_start,l);
        my_buf[l] = '\0';
        printf("%s\n",my_buf);
      }
      match_found = 0;
    }

    for(j=0;j<bn2;j++){
      if (bracket2[j].result==0 && !match_j[j] && DEBUG>1) {
        /* test bracket with no corresponding gold bracket */
        printf("  EXTRA[%d-%d]: ",bracket2[j].start,bracket2[j].end-1);
        l = bracket2[j].buf_end-bracket2[j].buf_start;
        strncpy(my_buf,buf+bracket2[j].buf_start,l);
        my_buf[l] = '\0';
        printf("%s\n",my_buf);
      }
    }

    /* crossing */
    /*----------*/
    crossing = 0;

    /* crossing is counted based on the brackets */
    /* in test rather than gold file (by Mike)   */
    for(j=0;j<bn2;j++){
      for(i=0;i<bn1;i++){
        if(bracket1[i].result != 5 &&
           bracket2[j].result != 5 &&
           ((bracket1[i].start < bracket2[j].start &&
             bracket1[i].end   > bracket2[j].start &&
             bracket1[i].end   < bracket2[j].end) ||
            (bracket1[i].start > bracket2[j].start &&
             bracket1[i].start < bracket2[j].end &&
             bracket1[i].end   > bracket2[j].end))){

          /* ### ML 09/01/03: get details on cross-brackettings */
          if (i != last_i) {
            if (DEBUG>1) {
                printf("  CROSSING[%d-%d]: ",bracket1[i].start,bracket1[i].end-1);
                l = bracket1[i].buf_end-bracket1[i].buf_start;
                strncpy(my_buf,buf1+bracket1[i].buf_start,l);
                my_buf[l] = '\0';
                printf("%s\n",my_buf);

                /* ML
                printf("\n  CROSSING at bracket %d:\n",i-1);
                printf("  GOLD (tokens %d-%d): ",bracket1[i].start,bracket1[i].end-1);
                l = bracket1[i].buf_end-bracket1[i].buf_start;
                strncpy(my_buf,buf1+bracket1[i].buf_start,l);
                my_buf[l] = '\0';
                printf("%s\n",my_buf);
                */
            }
            last_i = i;
          }

          /* ML
          printf("  TEST (tokens %d-%d): ",bracket2[j].start,bracket2[j].end-1);
          l = bracket2[j].buf_end-bracket2[j].buf_start;
          strncpy(my_buf,buf+bracket2[j].buf_start,l);
          my_buf[l] = '\0';
          printf("%s\n",my_buf);
          */

          crossing++;
          break;
        }
      }
    }
    /* Tagging accuracy */
    /*------------------*/
    correct_tag=0;
    for(i=0;i<wn1;i++){
       if(labelComp(terminal1[i].label,terminal2[i].label)==1){
          terminal1[i].result = terminal2[i].result = 1;
          correct_tag++;
       } else {
          terminal1[i].result = terminal2[i].result = 0;
       }
    }

    individualResult(wn1,r_bn1,r_bn2,match,crossing,correct_tag);
}

