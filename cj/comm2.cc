#include <iostream>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <errno.h>
bool portFree(int port)
{
    struct sockaddr_in serv_addr;
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if( sockfd < 0 ) 
        return false;
    //else 
    //    printf("Opened fd %d\n", sockfd);

    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(port);
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) 
    {
        if( errno == EADDRINUSE )
            return false;
        else 
            return false;
    }

    if (close (sockfd) < 0 )
        return false;

    return true;
}

int main(int argc, char **argv) 
{
    if( argc < 2 )
        return 0;
    int port = atoi(argv[1]);
    std::cout << port << '\t' << portFree(port) << '\n';
}
