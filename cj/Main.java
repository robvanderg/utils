import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;


public class Main {

    /**
     * @param args
     */
    public static void main(String[] args) {
        ServerSocket serverSocketIn = null; 
        Socket clientSocketIn = null;
         
         
        try {
            serverSocketIn = new ServerSocket(4447);
            clientSocketIn = serverSocketIn.accept();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } // 4447 is port number

        String inputLine; 
        BufferedReader in;
        
        try {
            in = new BufferedReader(new InputStreamReader(clientSocketIn.getInputStream()));
            while((inputLine = in.readLine()) != null){
                System.out.println(inputLine);
                BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(clientSocketIn.getOutputStream())); 
                wr.write(inputLine + " Back!");
                wr.flush(); // flushes the stream
            }

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } 
    }
}
