#include "charngram/charngram.h"

#include <iostream>
#include <string>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    CharNGram ngrams;
    ngrams.learn("/dev/shm/tweets.en");
    ngrams.saveBin("twitter.en.bin");
    ngrams.save("twitter.en.txt");

    /*CharNGram ngrams2("test.bin", true);
    ngrams2.saveBin("test.bin2");
    
    std::cout << ngrams.getCount('a') << '\n';
    std::cout << ngrams2.getCount('a') << '\n';
    std::cout << ngrams.getCount('a', 'n') << '\n';
    std::cout << ngrams2.getCount('a', 'n') << '\n';
    std::cout << ngrams.getCount('x', 'q') << '\n';
    std::cout << ngrams2.getCount('x', 'q') << '\n';
    std::cout << ngrams.getTotalChars() << '\n';
    std::cout << ngrams2.getTotalChars() << '\n';*/
}

