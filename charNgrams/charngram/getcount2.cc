#include "charngram.ih"

uint64_t CharNGram::getCount(char ch1, char ch2)
{
    return d_biCounts[(size_t)ch1 * d_size + (size_t)ch2];
}
