#ifndef INCLUDED_CHARNGRAM_
#define INCLUDED_CHARNGRAM_

#include <string>
#include <vector>
#include <stdint.h>

class CharNGram
{
    uint64_t d_size;
    std::vector<uint64_t> d_uniCounts; 
    std::vector<uint64_t> d_biCounts;
    uint64_t d_totalChars;

    public:
        CharNGram();
        CharNGram(std::string path, bool bin = false);
        
        void learn(std::string path);
        void save(std::string path);
        void load(std::string path);
        void saveBin(std::string path);
        void loadBin(std::string path);

        uint64_t getCount(char ch);
        uint64_t getCount(char ch1, char ch2);
        uint64_t getTotalChars();
    private:
};

#endif
