#include "charngram.ih"

void CharNGram::loadBin(string path)
{
    cerr << "Loading: " << path << '\n';

    ifstream ifs(path, ios::binary);
    if (!ifs.good())
    {
        cerr << "Could not read ngrams: " << path << "\n";
        exit(1);
    }

    //uint64_t size;
    ifs.read(reinterpret_cast<char*>(&d_totalChars), sizeof(uint64_t));
    ifs.read(reinterpret_cast<char*>(&d_uniCounts[0]), sizeof(uint64_t) * d_size);
    ifs.read(reinterpret_cast<char*>(&d_biCounts[0]), sizeof(uint64_t) * d_size * d_size);

    ifs.close();
}

