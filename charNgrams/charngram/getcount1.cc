#include "charngram.ih"

uint64_t CharNGram::getCount(char ch)
{
    return d_uniCounts[(size_t)ch];
}
