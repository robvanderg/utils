#include "charngram.ih"

CharNGram::CharNGram(string path, bool bin):
    CharNGram()
{
    if (bin)
        loadBin(path);
    else
        load(path);
}
