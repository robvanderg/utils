#include "charngram.ih"

void CharNGram::learn(string path)
{
    // read char by char
    size_t prevIdx = 0;
    char curCh = '\0';
    ifstream ifs(path);
    while (ifs.get(curCh))
    {
        //cout << curCh << '\n';
        ++d_totalChars;
        if (d_totalChars%1000000000 == 0)
            cout << d_totalChars << '\n';
        size_t curIdx = static_cast<unsigned int>(static_cast<unsigned char>(curCh));
        if (curIdx >= d_size)
        {
            cout << "ERROR\t-" << curCh << "-" << curIdx << '\n';
            continue;
        }
        ++d_uniCounts[curIdx];

        if (prevIdx != 0)
            ++d_biCounts[prevIdx * d_size + curIdx];
        prevIdx = curIdx;
    }
}
