#include "charngram.ih"

CharNGram::CharNGram()
{
    d_size = numeric_limits<unsigned char>::max();
    d_uniCounts = vector<uint64_t>(d_size);
    d_biCounts = vector<uint64_t>(d_size * d_size);
    d_totalChars = 0;
}
