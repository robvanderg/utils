#include "charngram.ih"

void CharNGram::saveBin(string path)
{
    cerr << "Writing: " << path << '\n';
    ofstream ofs(path, ios::binary);

    if (!ofs.good())
    {
        cerr << "Could not write ngrams: " << path << '\n';
        exit(1);
    }

    ofs.write(reinterpret_cast<char*>(&d_totalChars), sizeof(uint64_t));
    ofs.write(reinterpret_cast<char*>(&d_uniCounts[0]), sizeof(uint64_t) * d_size);
    ofs.write(reinterpret_cast<char*>(&d_biCounts[0]), sizeof(uint64_t) * d_size * d_size);

    ofs.close();
}

