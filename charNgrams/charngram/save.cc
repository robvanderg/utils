#include "charngram.ih"

void CharNGram::save(string path)
{
    cerr << "Writing: " << path << ".1\n";
    ofstream uniFile(path + ".1");
    if (!uniFile.good())
    {
        cerr << "Could not write unigrams: " << path << ".1\n";
        exit(1);
    }
    for (size_t uniIdx = 0; uniIdx != d_size; ++uniIdx)
        uniFile << d_uniCounts[uniIdx] << '\n';
    uniFile.close();
    
    cerr << "Writing: " << path << ".2\n";
    ofstream biFile(path + ".2");
    if (!biFile.good())
    {
        cerr << "Could not write bigrams: " << path << ".2\n";
        exit(1);
    }
    for (size_t biIdx = 0; biIdx != d_size * d_size; ++biIdx)
        biFile << d_biCounts[biIdx] << '\n';
    biFile.close();
}

