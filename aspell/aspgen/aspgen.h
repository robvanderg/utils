#ifndef INCLUDED_ASPGEN_
#define INCLUDED_ASPGEN_

#include <vector>
#include <string>

#include "../headers/aspell.h"

class AspGen
{
    std::vector<std::string> d_results;
    std::vector<double> d_vals;

    AspellCanHaveError * d_ret;
    AspellSpeller * d_speller;
    AspellConfig * d_config;

    public:
        AspGen(std::string lang="en_US", std::string path = "", std::string mode = "normal");
        ~AspGen();
       
        bool find(char word[]);
        bool find(std::string word);

        std::string *getCands(){return &d_results[0];};
        double *getVals(){return &d_vals[0];};
        size_t getNumCands(){return d_results.size();};
    private:
        void print_word_list(AspellSpeller * speller, 
                                const AspellWordList *wl, 
                                char delem);
};

#endif

