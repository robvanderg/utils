#include "aspgen.ih"

AspGen::AspGen(string lang, string path, string mode)
//:
{
    d_config = new_aspell_config();

    cerr << "Loading: " << path << '\n';
    aspell_config_replace(d_config, "data-dir", &path[0]);
    aspell_config_replace(d_config, "lang", &lang[0]);
    aspell_config_replace(d_config, "sug-mode", &mode[0]);
    aspell_config_replace(d_config, "encoding", "utf-8");
    d_ret = new_aspell_speller(d_config);
    d_speller = 0;
    if (aspell_error_number(d_ret) != 0)
        puts(aspell_error_message(d_ret));
    else
        d_speller = to_aspell_speller(d_ret);
}
