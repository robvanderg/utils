#include "aspgen.ih"

bool AspGen::find(char word[]) 
{
    const AspellSuggestionList * suggestions = aspell_speller_suggest_plus(
                                                      d_speller,
                                                      word, -1);
    AspellSuggestionEnumeration * elements =
                              aspell_suggestion_list_elements(suggestions);
    const AspellSuggestion * sugg;
    d_results.clear();
    d_vals.clear();
    while ( (sugg = aspell_suggestion_enumeration_next(elements)) != NULL )
    {
        d_results.push_back(sugg->word);
        d_vals.push_back(sugg->score);
    }
    delete_aspell_suggestion_enumeration(elements);
    return !d_results.empty();
}

