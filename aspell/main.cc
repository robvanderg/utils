
#include "aspgen/aspgen.h"
#include <vector>
#include <string>
#include <iostream>

int main(int argc, char *argv[])
{
    AspGen aspell("tr", "./aspell-tr-0.50-0/", "normal");

    for (int beg = 1; beg != argc; ++beg)
    {
        if(aspell.find(argv[beg]))
        {
            std::string *results = aspell.getCands();
            double *vals = aspell.getVals();
            for (size_t candIdx = 0; candIdx != aspell.getNumCands(); ++ candIdx)
                std::cout << candIdx << '\t' << results[candIdx] << '\t' << vals[candIdx] << '\n';
        }
    }
}

