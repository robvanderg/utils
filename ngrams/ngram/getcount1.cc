#include "ngram.ih"

uint64_t NGram::getCount(string word)
{
    uint32_t wordId = d_unigrams.getId(word.c_str());
    return (d_unigramCounts[wordId]);
}
