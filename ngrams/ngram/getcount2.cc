#include "ngram.ih"

uint32_t NGram::getCount(string word1, string word2)
{
    uint32_t word1Id = d_unigrams.getId(word1.c_str());
    uint32_t word2Id = d_unigrams.getId(word2.c_str());
    
    uint64_t bigramId = word1Id;
    bigramId <<= 32;
    bigramId += word2Id;

    uint32_t bigramIdx = findBigram(bigramId, 0, d_bigrams.size());
    return (bigramIdx == 0)? 0:d_bigramCounts[bigramIdx];
}
