#include "ngram.ih"

void NGram::loadBin(string path)
{
    cerr << "Loading: " << path << '\n';

    ifstream ifs(path, ios::binary);
    if (!ifs.good())
    {
        cerr << "Could not read ngrams: " << path << "\n";
        exit(1);
    }

    d_unigrams.loadBin(&ifs);

    uint64_t size;
    ifs.read(reinterpret_cast<char*>(&size), sizeof(uint64_t));
    d_unigramCounts = vector<uint64_t>(size);
    ifs.read(reinterpret_cast<char*>(&d_unigramCounts[0]), sizeof(uint64_t) * size);

    ifs.read(reinterpret_cast<char*>(&size), sizeof(uint64_t));
    d_bigrams = vector<uint64_t>(size);
    ifs.read(reinterpret_cast<char*>(&d_bigrams[0]), sizeof(uint64_t) * size);

    ifs.read(reinterpret_cast<char*>(&size), sizeof(uint64_t));
    d_bigramCounts = vector<uint32_t>(size);
    ifs.read(reinterpret_cast<char*>(&d_bigramCounts[0]), sizeof(uint32_t) * size);

    ifs.close();
}

