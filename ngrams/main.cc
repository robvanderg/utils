#include "ngram/ngram.h"

#include <iostream>
#include <string>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    /*NGram ngrams2;
    ngrams2.loadBin(argv[1]);
    ngrams2.save(argv[2]);
    exit(1);*/
    if (argc < 4)
    {
        std::cout << "Usage: give <srcFile> <dstFile> <minCount> \n";
        exit(1);
    }
    NGram ngrams;

    ngrams.learn(argv[1], std::stoi(argv[3])); 
    ngrams.save(argv[2]);
    ngrams.saveBin(std::string(argv[2]) + ".bin");
}

