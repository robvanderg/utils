#include <string>
#include <iostream>

int main()
{
    std::cout << sizeof(char) << '\n';
    std::cout << sizeof(wchar_t) << '\n';
    //std::string test = "😃😃😃 1Januari";
    std::string test = "😃";
    const char *test2 = test.c_str();
    const wchar_t *test3 = L"😃😃😃 1Januari";
    std::wstring test4 = L"😃";
    std::string test5(test2);
    for (size_t i = 0, iMax = test5.size(); i < iMax; ++i)
        std::cout << " " << static_cast<unsigned int>(static_cast<unsigned char>(test5[i]));
    std::cout << '\n'; 
    
    std::cout << "test: " << test << '\n';
    std::cout << "test2: " << test2 << '\n';
    std::cout << "test3: " << test3 << '\n';
    for (size_t i = 0; i != test4.size(); ++i)
        std::cout << test4[i] << '\t';
    std::cout << '\n';
    std::cout << "test5: " << test5 << '\n';
    std::cout << test.size() << '\n';
    std::cout << test4.size() << '\n';
    //std::cout << test4 << '\n';
}
