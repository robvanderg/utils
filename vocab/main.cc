#include "vocab/vocab.h"

#include <iostream>
#include <string>
#include <stdlib.h>
#include <fstream>
#include <utility>
#include <stdint.h>
#include <unordered_map>
#include <vector>
#include <sstream>

int main(int argc, char *argv[])
{
/*    Vocab test;
    test.loadOrdered("test.voc");
    std::ifstream ifs("test.voc");
    std::string word;
    size_t counter = 0;
    while(getline(ifs, word))
    {
        ++counter;
        if (test.getId(word) != counter)
            std::cout << test.getId(word) << '\t' << counter << '\t' <<  word << '\n';
    }*/

    Vocab newVoc;
    newVoc.load("/home/p270396/test.voc");


    std::ifstream ifs("/home/p270396/nl.cached");
    std::vector<std::string> oldVoc;
    int size;
    ifs >> size >> size;
    std::string line;
    for(int counter = 0; counter != size; ++counter)
    {
        getline(ifs, line);
        oldVoc.push_back(line);
    }
    
    std::vector<int> cands(newVoc.size() * 40);
    std::vector<double> dists(newVoc.size() * 40);
    while(getline(ifs, line))
    {
        std::istringstream iss(line);
        int wordId;
        double dist;
        iss >> wordId;
        std::string word = oldVoc[wordId];
        if (newVoc.contains(word))
        {
            int id = newVoc.getId(word); 
            for (int beg =0; beg!=40; ++beg)
            {
                iss >> wordId;
                iss >> dist;
                cands[id * 40 + beg] = wordId;
                dists[id * 40 + beg] = dist;
            }
        }
        else
        {
        }
    }
    ifs.close();
    std::cout << "oldVoc " << oldVoc.size() << '\n';
    std::cout << "newVoc " << newVoc.size() << '\n';

    std::ofstream ofs("/home/p270396/nl.new");
    ofs << 40 << '\n';
    ofs << newVoc.size() << '\n';
    newVoc.save(&ofs);
    for (int beg = 0; beg != newVoc.size(); ++beg)
    {
        ofs << beg;
        //std::cout << beg << '\n';
        //std::cout << beg << '\t' << newVoc.getWord(beg) << '\n';
        for (int candId = 0; candId != 40; ++candId)
        {
            int newCandId = 0;
            
            //std::cout << oldVoc[cands[beg * 40 + candId]] << '\t' << cands[beg * 40 + candId] << '\n';

            if ((beg * 40 + candId) >= cands.size())
                std::cout << "ERROR\n";
            int oldId = cands[beg * 40 + candId];
            if (oldId >= oldVoc.size())
            {
                std::cout << "ERROR2\n";
                std::cout << oldId << '\t' << oldVoc.size() << '\n';
                oldId = 0;
            }

            if (newVoc.contains(oldVoc[oldId]))
                newCandId = newVoc.getId(oldVoc[cands[beg * 40 + candId]]);
            else
                std::cout << "NF: " << beg << '\t' << candId << '\t' << oldVoc[cands[beg * 40 + candId]] << '\n';
            //std::cout << cands[beg * 40 + candId] << '\t' << newCandId << '\n';
            //std::cout << oldVoc[cands[beg * 40 + candId]] << '\t' << newVoc.getWord(newCandId) << '\n';
            ofs << '\t' << newCandId << '\t' << dists[beg * 40 + candId];
        }
        ofs << '\n';
    }
    ofs.close();
}

