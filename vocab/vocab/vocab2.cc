#include "vocab.ih"

Vocab::Vocab(string const &path, bool bin)
//:
{
    if (bin)
        loadBin(path);
    else
        load(path);
}
