#include "vocab.ih"

void Vocab::loadBin(string const &path)
{
    ifstream ifs(path, ios::binary);
    if (!ifs.good())
    {
        cerr << "Could not read vocab: " << path << '\n';
        exit(1);
    }

    loadBin(&ifs);
    ifs.close();
}
