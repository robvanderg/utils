#include "vocab.ih"

void Vocab::loadOrdered(string const &path)
{
    ifstream in(path);
    if (!in.good())
    {
        cerr << "Could not read vocab: " << path << '\n';
        exit(1);
    }
    loadOrdered(&in);
}
