#include "embeds/embeds.h"
#include "w2v/w2v.h"
#include "convert/convert.h"

#include <string>
#include <iostream>
#include <vector>

int main(int argc, char *argv[])
{

    /*Embeds test5;
    test5.combine(argc, argv);
    test5.saveTxt("test.txt");
    test5.saveBin("test.bin");*/
    if (argc == 6 or argc == 7)
    {
        int numCands = std::stoi(argv[2]);
        int threads = std::stoi(argv[3]);
        std::string dst(argv[4]);
        if (argc == 7)
        {
            int beg = std::stoi(argv[5]);
            int end = std::stoi(argv[6]);
            Convert myConverter(argv[1], numCands, threads, beg, end);
            myConverter.saveTxt(dst);
        }
        else 
        {
            std::string wordList(argv[5]);
            Convert myConverter(argv[1], numCands, threads, wordList);
            myConverter.saveTxt(dst);
        }
        exit(0);
    }
    /*else
    {
        std::cout << "usage:\n" 
        << argv[0] << " embeddings #cands #threads dst #beg #end\n"
        << "OR:\n"
        << argv[0] << " embeddings #cands #threads dst list\n";
        
        //exit(0);
    }*/

    Embeds test;
    test.loadTxt("test.txt");

    for (std::string testWord: { "drink", "\"0", "yolcusu..", "!"})
    {
        std::vector<std::string> resCands(40);
        std::vector<double> resVals(40);
        std::cout << testWord << ":\t" << test.find(testWord.c_str(), &resCands[0], &resVals[0]) << '\n';
        for (size_t i = 0; i != 10; ++i)
            std::cout << i << ". " << resCands[i] << '\t' << resVals[i] << '\n';        
    }
    //test.saveBin("/home/p270396/projects/codeSwitch/data/tr/w2v.bin.cache.bin");
    Embeds test2;
    test2.loadBin("test.bin");
    for (std::string testWord: {"drink"})
    {
        std::vector<std::string> resCands(40);
        std::vector<double> resVals(40);
        std::cout << testWord << ":\t" << test2.find(testWord.c_str(), &resCands[0], &resVals[0]) << '\n';
        for (size_t i = 0; i != 10; ++i)
            std::cout << i << ". " << resCands[i] << '\t' << resVals[i] << '\n';        
    }
}
