#include "convert.ih"

void Convert::saveTxt(string const &loc)
{
    ofstream ofs(loc + to_string(d_beg) + "-" + to_string(d_end));
    ofs << N << '\n' 
        << d_wordIds.size() << '\n';

    d_wordIds.save(&ofs);

    cout << "saving " << d_end << '\n';    
    for (uint32_t procId = d_beg; procId != d_end; ++procId)
    {
        ofs << procId  << '\t';
        for (uint16_t candId = 0; candId != N; ++candId)
           ofs << d_cands[(procId) * N + candId] << '\t' << d_vals[(procId) * N + candId] << '\t';
        ofs << '\n';
    }
    ofs.close();
}

