#ifndef INCLUDED_FINDER_
#define INCLUDED_FINDER_

#include <stdint.h>
#include <string>
#include <unordered_map>
#include "../../../vocab/vocab/vocab.h"

class Finder
{
    // == from w2v ==
    long long max_size; // max length of strings
    long long N; // number of closest words to save
    long long max_w; // max length of voc entries
    long long words; // size of vocab
    long long size; // number of vectors
    float *M; // vectors
    char *vocab; //vocabulary, uses max_w

    uint32_t *d_cands;
    double *d_vals;
    Vocab *d_wordIds;

    // == own ==
    char *src;
    uint32_t *cands;
    double *probs;
    long long a, b, c, d, cn;
    float dist, len;

    long long *bi;
    float *bestd; //TODO can be done in *probs?
    long long *bestw;
    float *vec;

    public:
        Finder(long long max_size, long long N, long long max_w, long long words, long long size, float *M, char*vocab, uint32_t *cands, double *vals, Vocab *wordIds);
        ~Finder();

        void find(uint32_t wordId);
    private:
};
        
#endif
