#ifndef INCLUDED_Convert_
#define INCLUDED_Convert_

#include <string>
#include <stdint.h>
#include <unordered_map>
#include <vector>
#include <queue>
#include <mutex>

#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "../../vocab/vocab/vocab.h"

class Convert
{
    long long max_size = 2000; // max length of strings
    long long N; // number of closest words to save
    long long max_w = 50; // max length of voc entries
    long long words; // size of vocab
    long long size; // size of vectors
    float *M; // vectors
    char *vocab; //vocabulary, uses max_w

    Vocab d_wordIds;
    uint32_t *d_cands;
    double *d_vals;

    std::queue<uint32_t> d_jobs;
    int d_numJobs;
    std::mutex d_jobMutex;

    Vocab d_knowns;
    uint32_t d_beg, d_end;

    public:
        Convert(std::string const &location, uint32_t numCands, uint16_t numThreads, std::string knowns);
        Convert(std::string const &location, uint32_t numCands, uint16_t numThreads, uint32_t beg, uint32_t end);

        void loadW2V(std::string const &loc);
        void init();
        void run(uint16_t numThreads);

        void saveTxt(std::string const &loc);
        void saveBin(std::string const &loc);
        void worker();

        void preLoad(std::string const &loc);
        void getJobs(size_t beg = 0, size_t end = 0);

    private:
        int parseLine(char* line);
        int getValue1();
        int getValue2();
};

#endif

