#include "convert.ih"

Convert::Convert(string const &location, uint32_t numCands, uint16_t numThreads, uint32_t beg, uint32_t end)
:
    N(numCands)
{
    loadW2V(location);
    init();
    getJobs(beg, end);
    run(numThreads);
}
