#include "convert.ih"

void Convert::getJobs(size_t beg, size_t end)
{
    d_beg = (beg == 0)? 1: beg;
    d_end = (end == 0)? d_wordIds.size(): end; 
    if (d_knowns.size() >= 1)
    {
        for (uint32_t wordId = 1; wordId != d_knowns.size() + 1; ++wordId)
        {
            string word = d_knowns.getWord(wordId);
            if (d_wordIds.contains(word))
                d_jobs.push(d_wordIds.getId(word));
        }
    }
    else
    {
        for (uint32_t procId = d_beg; procId != d_end; ++procId)
            if (d_cands[procId * N] == 0)
                d_jobs.push(procId);
    }

    d_numJobs = d_jobs.size();
    cerr << "Job size: " << d_jobs.size();
}
