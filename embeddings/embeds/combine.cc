#include "embeds.ih"

void Embeds::combine(int argc, char*argv[])
{
    // Get vocSize of embeds
    ifstream ifs(argv[1]);
    ifs >> d_numCands >> d_numWords;
    
    vector<string> idToWord = vector<string>(d_numWords + 2);
    map<string, int> wordToId;
    string word;
    uint32_t id;
    double dist;
    for (int beg = 0; beg != d_numWords; ++beg)
    {
        ifs >> word;
        if (word.size() == 0)
        {
            idToWord.push_back("");
            continue;
        }
        idToWord[beg+2] = word;
        wordToId.emplace(word, beg+2);
        d_vocab.addWord(word);
    }
    ifs.close();

    d_vocab.optimize();
    d_vocab.save("test.voc");
    cout << "prev size: " << d_numWords << '\n';
    cout << "new Vocab size: " << d_vocab.size() << '\t' << d_vocab.getWord(1000) << '\n';
    uint32_t oldSize = d_numWords;
    d_numWords = d_vocab.size() ;
    d_cands = vector<uint32_t>(d_numCands * d_numWords);
    d_vals = vector<double>(d_numCands * d_numWords);

    for (int fileIdx = 1; fileIdx != argc; ++fileIdx)
    {
        ifs.open(argv[fileIdx]);
        for (int vocIdx = 0; vocIdx != d_numWords + 2; ++vocIdx)
            getline(ifs,word); //to skip voc.
        
        //for (size_t beg =0; beg != 100000; ++beg)
        while(getline(ifs, word))
        {
            size_t oldId;
            istringstream iss(word);
            
            iss >> oldId;
            if (oldId >= oldSize)
                continue;
            size_t newId = d_vocab.getId(idToWord[oldId]);
            
            for (int candIdx = 0; candIdx != d_numCands; ++candIdx)
            {
                size_t oldCandId;
                iss >> oldCandId >> dist;
                size_t newCandId = d_vocab.getId(idToWord[oldCandId]);
                d_cands[newId * d_numCands + candIdx] = newCandId;
                d_vals[newId * d_numCands + candIdx] = dist;
            }
            //cout << idToWord[oldId] << '\t' << d_cands[newId * d_numCands] << " - " << d_vals[newId * d_numCands] << '\n';
        }
        ifs.close();
    }
    cout << "done\n";
}

