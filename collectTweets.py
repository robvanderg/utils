import os
import json
import gzip
import sys

if len(sys.argv) < 2:
    print('please give language string as argv[1]')
    exit(1)

startDir = '/net/corpora/twitter/000WORLD/000RAW'

for month in os.listdir(startDir):
    for hour in os.listdir(os.path.join(startDir, month)):
        if not hour.endswith('out.gz'):
            continue
        hourFile = os.path.join(startDir, month, hour)
        for line in gzip.open(hourFile, 'rt'):
            lineStr = str(line).strip()
            lineStr = lineStr[:lineStr.rfind('}')+1]
            try:
                data = json.loads(lineStr)
            except:
                continue
            if 'text' not in data:
                continue
            text = data['text']
            lang = data['user']['lang']
            if lang == sys.argv[1]:
                print(text)

